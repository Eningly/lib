var gulp = require('gulp'),
	// jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat');

gulp.task('default', function() {
  return gulp.src(['src/core/*.js','src/modules/**/*.js'])
  	// .pipe(jshint('.jshintrc'))
   //  .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('tmp'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('tmp'))
});