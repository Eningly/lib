STACK.Print = function (config) {
	config = config !== undefined ? config : {};
	this._file = config.file ? config.file : null;
	this._printFile = true;
	this._zelf = false;
	this._css = config.css !== undefined ? config.css : null;
	this._cssA = true;
	this._toBuild = config.parts !== undefined ? config.parts : null;
	this._partsCheck = true;
	if (!this._css) {
		this._cssA = false;
	}
	if (!this._toBuild) {
		this._partsCheck = false;
	}
	if (!this._file) {
		console.warn('STACK.Print: No File Given');
		this._printFile = false;
		this._self = true;
	}
	if (!this._self) {
		this._htmlToBuild = '';
		this._first();
	}
}